Pod::Spec.new do |s|
  s.name                = "FreestarAds-AdQualityKit"
  s.version             = "5.0.4"
  s.author              = 'Freestar'
  s.license             = { :type => 'Commercial', :text =>  'Copyright 2022 Publisher First, Inc. dba Freestar' }
  s.homepage            = 'http://www.freestar.com'
  s.summary             = 'Freestar Ads Ad Quality SDK'
  s.platform            = :ios, '11.0'
  s.source              = { :http => 'https://gitlab.com/freestar/ios-freestar-adquality/-/raw/5.0.4/ConfiantSDK.xcframework.zip' }
  s.vendored_frameworks = 'ConfiantSDK.xcframework'

end
